# Welcome to my  Profile :wink:

## __Introduction__

#### __Biography__

<img src="Assets/zaim.png" alt="screenshot" width="300"/>

Here is some of the introduction of me, I will list some of my biography by using a point below:
- Name : Wan Ahmad Zaim BIn Wan Roslan
- Age : 22
- Matric no : 198932
- Course : Bachelor Of Aerospace Engineering with Honours 
- Linkedin : [linkedin.com/in/wanahmadzaim](https://www.linkedin.com/in/wanahmadzaim/)

#### __My Strength__
Under this section, I will list all of my *__My Strength__* by using a numerical list:

1. Can adapt in any situation
2. Easy going
3. Lot of idea (maybe)

#### __My Weaknesses__
Under this section, I will list all of my *__My Weaknesses__* by using a numerical list:

1. Overthink something that not really important
2. Annoying
3. ~~Not Handsome~~

---

## __Project__

#### __How to use GitLab__
GitLab are use to store any codes and maybe development of the project documentation online. The way to use it similar on how to develop a website, but GitLab is more easier which we dont have to do extra work on the CSS, Json or the backend development. This GitLab just using a very simple language which called "Markdown Language". Below the i will expalain on how to use it in a very simple way.

##### __The step__

> __Creating a Project__
>> __Create a main script to display anything as main__
>>> __All the script can be typed by using "markdown" language__
>>>> __Commit changes on the main, but if we are working with team, we need to change anything in our own branches then we need to request merge (then later the Supervisor will merge the changes)__
>>>>> __Then all the changes will be dispaly on the main preview.__

Note: To add a picture, you need to create a "New Directory" as a new file to stored all the assets. This will be very usefull to link the path in the script on the script development. 

##### __The Markdown Language__

Unlike html, this languae is very straight foward to understand and very easy to use. GitLab just want to stored all the coding and documentation of a project, so this language will be used in the development of the script in order to preview to the user. 



##### __Basic markdown__

In the code stated above, there is an explanation for it. To explained all the element on that simple python code, i will list and explain it by using a table below:

| Element | Description |
| ------ | ----------- |
| # Text | Heading 1 |
| ## Text | Heading 2 |
| ### Text | Heading 3 |
| #### Text | Heading 4 |
| ##### Text | Heading 5 |
| ###### Text | Heading 6 |
| __ Bold __ | __Bold__ |
| _ Italic _ | _Italic_|
| ** Bold ** | **Bold** |
| * Italic * | *Italic* |
| ~~ CrossThrough ~~ | ~~CrossThrough~~ |

An extra element that fun to try are:

| Element | Preview |
| ------ | ----------- |
| : wink :  | :wink: |
| : cry : | :cry: |
| : laughing : | :laughing: |
| : yum : | :yum: |


Under this section I will show the code to make a table:

```
| Element | Preview |
| ------ | ----------- |
| : wink :  | :wink: |
| : cry : | :cry: |
| : laughing : | :laughing: |
| : yum : | :yum: |
```

Under this section I will show the code to make a list for numerical or a point list:

```
Point List
- list 1
- list 2
- list 3

Numerical List
1. list 1
2. list 2
3. list 3
```

to insert a picture and to resize it:

```
<img src="Assets/zaim.png" alt="screenshot" width="300"/>
```

to make a clickable link:

```
[the shortform that clickable](paste the link here)

[linkedin.com/in/wanahmadzaim](https://www.linkedin.com/in/wanahmadzaim/)
```

for an extra, maybe you can go to [Markdown Language Guide](https://www.markdownguide.org/)  
try and have fun :wink:

---

##### __Group working__
While working in groups, the crucial part to trace the changes. So the "__Branches__" is a crucial part to do. If we do create our own branches, all the current file that associate on the "__main.md__" will be copied for our own branches and allow us to edit anything from that. So the simple way to do this is:

1. Create a new brances by using our own name (by clicking the "+")
2. Make sure the branches name is our own name
3. You can edit any file on that branches by clicking "Web IDE" or "Edit"
4. Click on "Commit"
5. Click on "Request Merge" 

it was very easy, and go try now on our [IDP Project](https://gitlab.com/putraspace/idp-2021)

## __Conclusion__

So far, i think that markdown language is very easy to use and it understandable, and this GitLab is very good start to understand how the development of websites due to a lot of similarities on the process (creating the folder/files in local desktop > coding the HTML but his GitLab just using Markdown > upload to server to storage the codes > and display the UI) but the differences is the link/domain is free and can easily develop at the very first of the project creation. hehe that all from me thx. 



